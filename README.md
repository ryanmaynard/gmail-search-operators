# 📧 [Gmail Search Operators][site]

<img src="assets/gmail.png" alt="gmail icon" width="150"/>

[![Netlify Status](https://api.netlify.com/api/v1/badges/066330ef-c521-47c1-9581-df0f837f9aac/deploy-status)](https://app.netlify.com/sites/gmailsearchoperators/deploys)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

# <img src="demo.png" alt="demo screenshot" width="600"/>

## 📜 Description

Welcome to **Gmail Search Operators**, your quick reference guide for mastering the art of searching through your Gmail inbox! ✨ This handy tool helps you streamline your email management with powerful search operators, making your email experience more efficient and productive. 🚀

## 🌟 Features

- 🔍 **Comprehensive Search Operators**: Access a wide range of search operators to refine your email searches.
- 📚 **Quick Reference Guide**: Easy-to-use reference for fast lookups.
- ⚡ **Efficiency Boost**: Save time and find emails faster with precise search queries.

## 🛠️ Getting Started

To get started, visit [Gmail Search Operators][site] and start exploring the search operators that can supercharge your email management. 

## 🤝 Contributing

We welcome contributions from the community! Be sure to review the [Contributing Guide][contributing] before submitting a Merge Request. Here's how you can contribute:

1. 🍴 Fork it: `https://gitlab.com/ryanmaynard/gmail-search-operators/forks/new`
2. 🌿 Create your feature branch: `git checkout -b my-new-feature`
3. 💾 Commit your changes: `git commit -am 'Add some feature'`
4. 📤 Push to the branch: `git push origin my-new-feature`
5. 🔄 Create a new Merge Request: `https://gitlab.com/ryanmaynard/gmail-search-operators/merge_requests/new`

## 🛡️ License

This project is licensed under the [MIT License](https://opensource.org/licenses/MIT). For a summary, check out the [MIT TLDR][tldr]. You can read the full license text [here][license].

## 📞 Contact

Have questions or suggestions? Feel free to reach out via the [Gmail Search Operators][site] website or open an issue on GitLab. 

## 🚀 Acknowledgments

- Thanks to the contributors who help keep this project up-to-date and useful! 🙌
- Inspired by the need for efficient email management. 📧

---

🌐 **Visit us at [Gmail Search Operators][site] and streamline your Gmail experience today!** 🌐

[site]: https://gmailsearchoperators.xyz
[contributing]: CONTRIBUTING.md
[tldr]: https://tldrlegal.com/license/mit-license
[license]: https://gitlab.com/ryanmaynard/mixtape/blob/master/LICENSE
